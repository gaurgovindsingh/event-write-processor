package com.gumgum.eventwriteprocessor;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Controller;

import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gumgum.eventwriteprocessor.config.ScyllaClient;

@Controller
public class EventWriter {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventWriter.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final ScyllaClient SCYLLA_CLIENT = new ScyllaClient();
    private static final Properties PROPERTIES = new Properties();

    @Value("${cloud.aws.end-point.uri}")
    private String sqsUrl;
    @Value("${cloud.aws.region.static}")
    private String awsRegion;

    private AmazonSQS amazonSQS;
    private static String region;

    @PostConstruct
    private void postConstructor() {
        region = awsRegion;
//        this.amazonSQS = AmazonSQSClientBuilder.standard()
//                .withRegion(awsRegion)
//                .withCredentials(new EnvironmentVariableCredentialsProvider())
//                .build();
        this.amazonSQS = AmazonSQSClientBuilder.defaultClient();
        initializeProperties();
        initializeScylla();
        LOGGER.info("Application initialized");
    }

    @EventListener(ApplicationReadyEvent.class)
    public void handleRequest() {
        final ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(sqsUrl)
                .withMaxNumberOfMessages(10)
                .withWaitTimeSeconds(2);

        while (true) {

            final List<Message> messages = amazonSQS.receiveMessage(receiveMessageRequest).getMessages();
            LOGGER.info("Received Messages: {}", messages.size());

            for (Message message : messages) {
                try {
                    JsonNode jsonNode = OBJECT_MAPPER.readTree(message.getBody());
                    LOGGER.info("Received Message: {}", jsonNode.get("message"));
                    deleteMessage(message);
                } catch (JsonProcessingException e) {
                    LOGGER.error("Error parsing message: {}", message);
                } catch (Exception e) {
                    LOGGER.error("Error processing message: {}", message, e);
                }
            }
            // Delay for Scylla writes
            try {
                TimeUnit.MILLISECONDS.sleep(50);
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
            }
        }
    }

    private static void initializeProperties() {
        String propertiesFile = region + ".properties";
        if ("dev".equals(System.getenv("ENV"))) {
            propertiesFile = "dev.properties";
        }
        try {
            InputStream inputStream = EventWriter.class.getClassLoader().getResourceAsStream(propertiesFile);
            PROPERTIES.load(inputStream);
        } catch (IOException e) {
            LOGGER.error("Error loading properties: {}", propertiesFile);
        }
    }

    private static void initializeScylla() {
        SCYLLA_CLIENT.setNodes(PROPERTIES.getProperty("scylla.seeds"));
        SCYLLA_CLIENT.setUsername(PROPERTIES.getProperty("scylla.username"));
        SCYLLA_CLIENT.setPassword(getScyllaPassword());
        SCYLLA_CLIENT.build();
    }

    private static String getScyllaPassword() {
        AWSSecretsManager client = AWSSecretsManagerClientBuilder.standard()
                .withRegion(region)
                .build();
        GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest()
                .withSecretId(PROPERTIES.getProperty("scylla.passkey"));
        try {
            GetSecretValueResult getSecretValueResult = client.getSecretValue(getSecretValueRequest);
            JsonNode jsonNode = OBJECT_MAPPER.readTree(getSecretValueResult.getSecretString());
            return getString(jsonNode, PROPERTIES.getProperty("scylla.username"));
        } catch (Exception e) {
            LOGGER.error("Error getting scylla password", e);
        }
        return null;
    }

    private static String getString(JsonNode jsonNode, String fieldName) {
        return jsonNode.get(fieldName).asText();
    }

    private static Long getLong(JsonNode jsonNode, String fieldName) {
        return jsonNode.get(fieldName).asLong();
    }

    private void deleteMessage(Message messageObject) {
        final String messageReceiptHandle = messageObject.getReceiptHandle();
        amazonSQS.deleteMessage(new DeleteMessageRequest(sqsUrl, messageReceiptHandle));
    }
}
