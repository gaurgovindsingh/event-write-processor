package com.gumgum.eventwriteprocessor.config;

import static com.datastax.driver.core.ConsistencyLevel.LOCAL_ONE;
import static com.gumgum.eventwriteprocessor.config.AppConstants.KEYSPACE;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.HostDistance;
import com.datastax.driver.core.PlainTextAuthProvider;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ProtocolOptions;
import com.datastax.driver.core.QueryOptions;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.policies.DCAwareRoundRobinPolicy;
import com.datastax.driver.core.policies.DefaultRetryPolicy;
import com.datastax.driver.core.policies.EC2MultiRegionAddressTranslator;
import com.datastax.driver.core.policies.TokenAwarePolicy;

public class ScyllaClient {
    private Cluster cluster;
    private Session session;
    private String nodes;
    private String username;
    private String password;

    public void build() {
        Cluster.Builder builder = Cluster.builder();
        builder.withAddressTranslator(new EC2MultiRegionAddressTranslator());
        builder.withCompression(ProtocolOptions.Compression.SNAPPY);
        builder.withQueryOptions(new QueryOptions().setConsistencyLevel(LOCAL_ONE));
        builder.withLoadBalancingPolicy(new TokenAwarePolicy(DCAwareRoundRobinPolicy.builder().build()))
                .withRetryPolicy(DefaultRetryPolicy.INSTANCE);
        PoolingOptions poolingOptions = new PoolingOptions();
        configurePoolingOptions(poolingOptions);
        builder.withPoolingOptions(poolingOptions);

        cluster = builder.addContactPoints(nodes.split(","))
                .withAuthProvider(new PlainTextAuthProvider(username, password))
                .withCompression(ProtocolOptions.Compression.LZ4)
                .build();
        session = cluster.connect(KEYSPACE);

    }

    public PreparedStatement prepare(String preparedStatementStr) {
        PreparedStatement preparedStatement = session.prepare(preparedStatementStr);
        return preparedStatement.setConsistencyLevel(LOCAL_ONE);
    }

    public ResultSet execute(BatchStatement batchStatement) {
        return session.execute(batchStatement);
    }

    private void configurePoolingOptions(PoolingOptions poolingOptions) {
        poolingOptions.setCoreConnectionsPerHost(HostDistance.LOCAL, Integer.parseInt(System.getenv("coreConnectionsLocal")));
        poolingOptions.setMaxConnectionsPerHost(HostDistance.LOCAL, Integer.parseInt(System.getenv("maxConnectionsLocal")));
        poolingOptions.setMaxRequestsPerConnection(HostDistance.LOCAL, Integer.parseInt(System.getenv("maxRequestsPerConnectionLocal")));
        poolingOptions.setCoreConnectionsPerHost(HostDistance.REMOTE, Integer.parseInt(System.getenv("coreConnectionsRemote")));
        poolingOptions.setMaxConnectionsPerHost(HostDistance.REMOTE, Integer.parseInt(System.getenv("maxConnectionsRemote")));
        poolingOptions.setMaxRequestsPerConnection(HostDistance.REMOTE, Integer.parseInt(System.getenv("maxRequestsPerConnectionRemote")));
        poolingOptions.setPoolTimeoutMillis(Integer.parseInt(System.getenv("poolTimeoutMillis")));
        poolingOptions.setMaxQueueSize(Integer.parseInt(System.getenv("maxQueueSize")));
    }

    public void setNodes(String nodes) {
        this.nodes = nodes;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
