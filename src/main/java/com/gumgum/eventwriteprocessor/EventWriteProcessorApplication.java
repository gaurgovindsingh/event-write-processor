package com.gumgum.eventwriteprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;

@SpringBootApplication(exclude = {ContextStackAutoConfiguration.class})
public class EventWriteProcessorApplication {

	public static void main(String[] args) {
		SpringApplication.run(EventWriteProcessorApplication.class, args);
	}

}
