//package com.gumgum.eventwriteprocessor;
//
//import javax.annotation.PostConstruct;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.messaging.support.MessageBuilder;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
//import com.amazonaws.services.sqs.AmazonSQSAsync;
//import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
//
//@RestController
//public class SendEvent {
//
//    @Value("${cloud.aws.end-point.uri}")
//    private String endpoint;
//    @Value("${cloud.aws.region.static}")
//    private String awsRegion;
//
//    private AmazonSQSAsync amazonSQSAsync;
//    private QueueMessagingTemplate queueMessagingTemplate;
//
//    @PostConstruct
//    public void init() {
//        amazonSQSAsync = AmazonSQSAsyncClientBuilder.standard().withRegion(awsRegion)
//                .withCredentials(new EnvironmentVariableCredentialsProvider())
//                .build();
//            amazonSQSAsync = AmazonSQSClientBuilder.defaultClient();
//
//        queueMessagingTemplate = new QueueMessagingTemplate(amazonSQSAsync);
//    }
//
//    @PostMapping("/send")
//    public ResponseEntity<String> sendMessageToQueue(@RequestBody String message) {
//        queueMessagingTemplate.send(endpoint, MessageBuilder.withPayload(message).build());
//        return new ResponseEntity<>(
//                HttpStatus.OK);
//    }
//}
